#!/usr/bin/env bash
# Common functions for multople scripts

set_types(){
	# Translate ISO type into architecture + UEFI flag
	if [ "${type:0:2}" = "32" ]; then
		arch="i586"
		lib="lib"
		uefi="${type:2:1}"
	elif [ "${type:0:4}" = "i586" ]; then
		# Support architecture instead of bitness, but consider it deprecated
		arch="i586"
		lib="lib"
		uefi="${type:4:1}"
	elif [ "${type:0:2}" = "64" ]; then
		arch="x86_64"
		lib="lib64"
		uefi="${type:2:1}"
	elif [ "${type:0:6}" = "x86_64" ]; then
		# Support architecture instead of bitness, but consider it deprecated
		arch="x86_64"
		lib="lib64"
		uefi="${type:6:1}"
	else
		echo ""
		echo "!!!         INVALID ISO TYPE SPECIFIED ($type), SKIPPING         !!!"
		echo ""
		num_fail=$((num_fail + 1))
	fi 
}

find_requires_ALT(){
# TODO: this is bad code!
# On ALT Linux, bash can do --rpm-requires, we don't have or use it on ROSA,
# but it can be used to make a list of build dependencies.
# This function is ran manually by maintainers of these scripts on ALT Linux,
# ROSA's root is considered to be mounted at /mnt/rosa-2016.1/
# To run it, execute from root shell:
# . ./common-funcs.sh && find_requires_ALT
# We assume that all required packages are already installed in your ROSA system.
	for file in MATRIX build
	do
		while read -r line
		do
			if [ ! -f "$line" ] && [ "$line" != "livecd-creator" ]; then
				line_chroot="$(chroot /mnt/rosa-2016.1 /usr/bin/which "$line")"
				req_list="${req_list} $(chroot /mnt/rosa-2016.1 /bin/sh -c "rpm -qf --queryformat='%{NAME}\n' ${line_chroot}")"
			fi
		done < <(bash --rpm-requires "$file" | grep ^executable | awk -F '(' '{print $NF}' | tr -d '()' | grep -v '^\/' | sort -u)
	done
	echo "$req_list" | tr ' ' '\n' | sort -u | tr '\n' ' '
	echo ""
}
